import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:smi_covid19id/models/dataGlobal.dart';
import 'package:smi_covid19id/models/dataGlobalPositive.dart';
import 'package:smi_covid19id/models/dataGlobalRecover.dart';
import 'package:smi_covid19id/models/dataGlobalDied.dart';
import 'package:smi_covid19id/models/dataByCountry.dart';
import 'package:smi_covid19id/models/dataByProvince.dart';

class Api {
  static final baseUrl = 'https://api.kawalcorona.com/';

  List<DataGlobal> listDataGlobal = [];
  List<DataByProvince> listDataByProvince = [];

  Future<List<DataGlobal>> fetchGlobal() async {
    listDataGlobal.clear();

    final http.Response response = await http.get(baseUrl);

    if (response.statusCode == 200) {
      final listData = jsonDecode(response.body);

      listData.forEach((data) => listDataGlobal.add(DataGlobal.fromJson(data)));

      return listDataGlobal;
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<DataGlobalPositive> fetchGlobalPositive() async {
    final http.Response response = await http.get(baseUrl + 'positif');

    if (response.statusCode == 200) {
      return DataGlobalPositive.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<DataGlobalRecover> fetchGlobalRecover() async {
    final http.Response response = await http.get(baseUrl + 'sembuh');

    if (response.statusCode == 200) {
      return DataGlobalRecover.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<DataGlobalDied> fetchGlobalDied() async {
    final http.Response response = await http.get(baseUrl + 'meninggal');

    if (response.statusCode == 200) {
      return DataGlobalDied.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<DataByCountry> fetchByCountry(String country) async {
    final http.Response response = await http.get(baseUrl + country);

    if (response.statusCode == 200) {
      return DataByCountry.fromJson(json.decode(response.body)[0]);
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<List<DataByProvince>> fetchByProvince() async {
    listDataByProvince.clear();

    final http.Response response = await http.get(baseUrl + 'indonesia/provinsi');

    if (response.statusCode == 200) {
      final listData = jsonDecode(response.body);

      listData.forEach((data) => listDataByProvince.add(DataByProvince.fromJson(data)));

      return listDataByProvince;
    } else {
      throw Exception('Failed to load data');
    }
  }
}
