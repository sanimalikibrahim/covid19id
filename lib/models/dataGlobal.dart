class DataGlobal {
  Attributes attributes;

  DataGlobal({this.attributes});

  factory DataGlobal.fromJson(Map<String, dynamic> json) {
    return DataGlobal(attributes: Attributes.fromJson(json['attributes']));
  }
}

class Attributes {
  int oBJECTID;
  String countryRegion;
  int lastUpdate;
  int confirmed;
  int deaths;
  int recovered;
  int active;

  Attributes({
    this.oBJECTID,
    this.countryRegion,
    this.lastUpdate,
    this.confirmed,
    this.deaths,
    this.recovered,
    this.active,
  });

  factory Attributes.fromJson(Map<String, dynamic> json) {
    return Attributes(
      oBJECTID: json['OBJECTID'],
      countryRegion: json['Country_Region'],
      lastUpdate: json['Last_Update'],
      confirmed: json['Confirmed'],
      deaths: json['Deaths'],
      recovered: json['Recovered'],
      active: json['Active'],
    );
  }
}
