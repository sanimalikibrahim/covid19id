class DataByProvince {
  Attributes attributes;

  DataByProvince({this.attributes});

  factory DataByProvince.fromJson(Map<String, dynamic> json) {
    return DataByProvince(attributes: Attributes.fromJson(json['attributes']));
  }
}

class Attributes {
  int fID;
  int kodeProvi;
  String provinsi;
  int kasusPosi;
  int kasusSemb;
  int kasusMeni;

  Attributes({this.fID, this.kodeProvi, this.provinsi, this.kasusPosi, this.kasusSemb, this.kasusMeni});

  factory Attributes.fromJson(Map<String, dynamic> json) {
    return Attributes(
      fID: json['FID'],
      kodeProvi: json['Kode_Provi'],
      provinsi: json['Provinsi'],
      kasusPosi: json['Kasus_Posi'],
      kasusSemb: json['Kasus_Semb'],
      kasusMeni: json['Kasus_Meni'],
    );
  }
}
