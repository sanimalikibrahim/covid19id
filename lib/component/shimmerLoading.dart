import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerLoading extends StatelessWidget {
  const ShimmerLoading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Shimmer.fromColors(
          baseColor: Colors.grey[300],
          highlightColor: Colors.grey[100],
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Row(children: [
              Container(
                width: 40.0,
                height: 40.0,
                // color: Colors.white,
                decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(100.0),
                      topRight: const Radius.circular(100.0),
                      bottomLeft: const Radius.circular(100.0),
                      bottomRight: const Radius.circular(100.0),
                    )),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      height: 25.0,
                      color: Colors.white,
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 2.0),
                    ),
                    Container(
                      width: 125.0,
                      height: 10.0,
                      color: Colors.white,
                    ),
                  ],
                ),
              )
            ]),
          )),
    );
  }
}
