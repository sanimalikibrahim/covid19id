import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Hotline extends StatelessWidget {
  const Hotline({Key key}) : super(key: key);

  Future<void> _openPhoneCallback(String phone) async {
    if (await canLaunch('tel:$phone')) {
      await launch('tel:$phone');
    } else {
      throw ('Tidak dapat membuka $phone');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(title: Text('Hotline')),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/double_bubble_outline.png'),
              repeat: ImageRepeat.repeat,
              colorFilter: ColorFilter.mode(Colors.white54.withOpacity(0.6), BlendMode.srcOver),
            ),
          ),
          child: Scrollbar(
            child: ListView(
              children: <Widget>[
                Card(
                  margin: EdgeInsets.all(20.0),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Coronavirus Hotline Indonesia',
                          style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(vertical: 4.0),
                        ),
                        Text(
                          'Layanan darurat via telepon yang disediakan oleh kemkes dan juga pemerintah provinsi',
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/kementrian_kesehatan.png'),
                    width: 40.0,
                  ),
                  title: Text(
                    '021-5210-411',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Kementrian Kesehatan'),
                  onTap: () {
                    _openPhoneCallback('021-5210-411');
                  },
                ),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/kementrian_kesehatan.png'),
                    width: 40.0,
                  ),
                  title: Text(
                    '0812-1212-3119',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Kementrian Kesehatan'),
                  onTap: () {
                    _openPhoneCallback('0812-1212-3119');
                  },
                ),
                Divider(),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/pemprov_dki.png'),
                    width: 40.0,
                  ),
                  title: Text(
                    '112',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Pemprov DKI Jakarta'),
                  onTap: () {
                    _openPhoneCallback('112');
                  },
                ),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/pemprov_dki.png'),
                    width: 40.0,
                  ),
                  title: Text(
                    '0813-8837-6955',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Pemprov DKI Jakarta'),
                  onTap: () {
                    _openPhoneCallback('0813-8837-6955');
                  },
                ),
                Divider(),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/pemprov_jateng.png'),
                    width: 40.0,
                  ),
                  title: Text(
                    '024-358-0713',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Pemprov Jawa Tengah'),
                  onTap: () {
                    _openPhoneCallback('024-358-0713');
                  },
                ),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/pemprov_jateng.png'),
                    width: 40.0,
                  ),
                  title: Text(
                    '0823-1360-0560',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Pemprov Jawa Tengah'),
                  onTap: () {
                    _openPhoneCallback('0823-1360-0560');
                  },
                ),
                Divider(),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/pemprov_jatim.png'),
                    width: 40.0,
                  ),
                  title: Text(
                    '031-843-0313',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Pemprov Jawa Timur'),
                  onTap: () {
                    _openPhoneCallback('031-843-0313');
                  },
                ),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/pemprov_jatim.png'),
                    width: 40.0,
                  ),
                  title: Text(
                    '0813-3436-7800',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Pemprov Jawa Timur'),
                  onTap: () {
                    _openPhoneCallback('0813-3436-7800');
                  },
                ),
                Divider(),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/pemprov_jabar.png'),
                    width: 40.0,
                  ),
                  title: Text(
                    '119',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Pemprov Jawa Barat'),
                  onTap: () {
                    _openPhoneCallback('119');
                  },
                ),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/pemprov_jabar.png'),
                    width: 40.0,
                  ),
                  title: Text(
                    '0811-209-3306',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Pemprov Jawa Barat'),
                  onTap: () {
                    _openPhoneCallback('0811-209-3306');
                  },
                ),
                Divider(),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/pemprov_yogya.jpg'),
                    width: 40.0,
                  ),
                  title: Text(
                    '0274-555-585',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Pemprov D.I Yogyakarta'),
                  onTap: () {
                    _openPhoneCallback('0274-555-585');
                  },
                ),
                ListTile(
                  leading: Image(
                    image: AssetImage('assets/pemprov_yogya.jpg'),
                    width: 40.0,
                  ),
                  title: Text(
                    '0811-2764-800',
                    style: TextStyle(fontSize: 16.5, fontWeight: FontWeight.w500),
                  ),
                  subtitle: Text('Pemprov D.I Yogyakarta'),
                  onTap: () {
                    _openPhoneCallback('0811-2764-800');
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
