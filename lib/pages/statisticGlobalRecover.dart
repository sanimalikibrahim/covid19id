import 'package:flutter/material.dart';
import 'package:smi_covid19id/component/shimmerLoading.dart';
import 'package:smi_covid19id/models/dataGlobalRecover.dart';
import 'package:smi_covid19id/services/api.dart';

class StatisticGlobalRecover extends StatefulWidget {
  @override
  _StatisticGlobalRecover createState() => _StatisticGlobalRecover();
}

class _StatisticGlobalRecover extends State<StatisticGlobalRecover> {
  Future<DataGlobalRecover> futureDataGlobalRecover;

  void initState() {
    super.initState();
    futureDataGlobalRecover = Api().fetchGlobalRecover();
  }

  Container _listDataItem(AsyncSnapshot snapshot) {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            leading: ExcludeSemantics(
              child: CircleAvatar(
                child: Icon(
                  Icons.sentiment_satisfied,
                  color: Colors.white,
                ),
                backgroundColor: Colors.green,
              ),
            ),
            title: Row(
              children: <Widget>[
                Text(snapshot.data.value, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20.0)),
                Text(' Orang'),
              ],
            ),
            subtitle: Text('Sembuh'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: futureDataGlobalRecover,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return _listDataItem(snapshot);
        } else if (snapshot.hasError) {
          return Padding(
            padding: const EdgeInsets.all(50.0),
            child: Center(child: Text(snapshot.error)),
          );
        }

        // Default, show loading
        return ShimmerLoading();
      },
    );
  }
}
