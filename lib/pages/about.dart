import 'package:flutter/material.dart';

class About extends StatelessWidget {
  const About({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(title: Text('Tentang')),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/double_bubble_outline.png'),
              repeat: ImageRepeat.repeat,
              colorFilter: ColorFilter.mode(Colors.white54.withOpacity(0.6), BlendMode.srcOver),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image(
                      image: AssetImage('assets/atom_128px.png'),
                      width: 64,
                      height: 64,
                      fit: BoxFit.contain,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                    ),
                    Text(
                      'Covid-19 ID',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 3.0),
                    ),
                    Text(
                      'Versi 1.0',
                      style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        children: <Widget>[
                          Text(
                            'Hoyaaah!',
                            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w400),
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 3.0),
                          ),
                          Text(
                              'Aplikasi ini dibuat hanya untuk portofolio pengembang saja. Tidak akan ada pembaruan baik untuk fitur ataupun perbaikan aplikasi.'),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 15.0),
                          ),
                          Row(
                            children: <Widget>[
                              Text('Email'),
                              const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 16.0),
                              ),
                              Text(
                                'sanimalikibrahim@gmail.com',
                                style: TextStyle(fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 1.5),
                          ),
                          Row(
                            children: <Widget>[
                              Text('Whatsapp'),
                              const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 4.0),
                              ),
                              Text(
                                '+62 813-9517-9452',
                                style: TextStyle(fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 1.5),
                          ),
                          Row(
                            children: <Widget>[
                              Text('Blog'),
                              const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 19.0),
                              ),
                              Text(
                                'developid.blogspot.com',
                                style: TextStyle(fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 4.0),
                          ),
                          Divider(),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 4.0),
                          ),
                          Text('Special thanks for kawalcorona.com'),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
