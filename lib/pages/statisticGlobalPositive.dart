import 'package:flutter/material.dart';
import 'package:smi_covid19id/component/shimmerLoading.dart';
import 'package:smi_covid19id/models/dataGlobalPositive.dart';
import 'package:smi_covid19id/services/api.dart';

class StatisticGlobalPositive extends StatefulWidget {
  @override
  _StatisticGlobalPositive createState() => _StatisticGlobalPositive();
}

class _StatisticGlobalPositive extends State<StatisticGlobalPositive> {
  Future<DataGlobalPositive> futureDataGlobalPositive;

  void initState() {
    super.initState();
    futureDataGlobalPositive = Api().fetchGlobalPositive();
  }

  Container _listDataItem(AsyncSnapshot snapshot) {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            leading: ExcludeSemantics(
              child: CircleAvatar(
                child: Icon(
                  Icons.sentiment_dissatisfied,
                  color: Colors.white,
                ),
                backgroundColor: Colors.red,
              ),
            ),
            title: Row(
              children: <Widget>[
                Text(snapshot.data.value, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20.0)),
                Text(' Orang'),
              ],
            ),
            subtitle: Text('Positif'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: futureDataGlobalPositive,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return _listDataItem(snapshot);
        } else if (snapshot.hasError) {
          return Padding(
            padding: const EdgeInsets.all(50.0),
            child: Center(child: Text(snapshot.error)),
          );
        }

        // Default, show loading
        return ShimmerLoading();
      },
    );
  }
}
