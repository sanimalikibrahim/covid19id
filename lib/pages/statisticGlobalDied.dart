import 'package:flutter/material.dart';
import 'package:smi_covid19id/component/shimmerLoading.dart';
import 'package:smi_covid19id/models/dataGlobalDied.dart';
import 'package:smi_covid19id/services/api.dart';

class StatisticGlobalDied extends StatefulWidget {
  @override
  _StatisticGlobalDied createState() => _StatisticGlobalDied();
}

class _StatisticGlobalDied extends State<StatisticGlobalDied> {
  Future<DataGlobalDied> futureDataGlobalDied;

  void initState() {
    super.initState();
    futureDataGlobalDied = Api().fetchGlobalDied();
  }

  Container _listDataItem(AsyncSnapshot snapshot) {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            leading: ExcludeSemantics(
              child: CircleAvatar(
                child: Icon(
                  Icons.sentiment_very_dissatisfied,
                  color: Colors.white,
                ),
                backgroundColor: Colors.pink,
              ),
            ),
            title: Row(
              children: <Widget>[
                Text(snapshot.data.value, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20.0)),
                Text(' Orang'),
              ],
            ),
            subtitle: Text('Meninggal'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: futureDataGlobalDied,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return _listDataItem(snapshot);
        } else if (snapshot.hasError) {
          return Padding(
            padding: const EdgeInsets.all(50.0),
            child: Center(child: Text(snapshot.error)),
          );
        }

        // Default, show loading
        return ShimmerLoading();
      },
    );
  }
}
