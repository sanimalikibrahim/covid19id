import 'package:flutter/material.dart';
import 'package:smi_covid19id/component/shimmerLoading.dart';
import 'package:smi_covid19id/models/dataByCountry.dart';
import 'package:smi_covid19id/services/api.dart';

class StatisticByCountry extends StatefulWidget {
  @override
  _StatisticByCountry createState() => _StatisticByCountry();
}

class _StatisticByCountry extends State<StatisticByCountry> {
  Future<DataByCountry> futureDataByCountry;

  void initState() {
    super.initState();
    futureDataByCountry = Api().fetchByCountry('indonesia');
  }

  Container renderDataItem(AsyncSnapshot snapshot) {
    return Container(
      child: Column(
        children: <Widget>[
          ListTile(
            leading: ExcludeSemantics(
              child: CircleAvatar(
                child: Icon(
                  Icons.sentiment_dissatisfied,
                  color: Colors.white,
                ),
                backgroundColor: Colors.red,
              ),
            ),
            title: Row(
              children: <Widget>[
                Text(snapshot.data.positif, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20.0)),
                Text(' Orang'),
              ],
            ),
            subtitle: Text('Positif'),
          ),
          ListTile(
            leading: ExcludeSemantics(
              child: CircleAvatar(
                child: Icon(
                  Icons.sentiment_satisfied,
                  color: Colors.white,
                ),
                backgroundColor: Colors.green,
              ),
            ),
            title: Row(
              children: <Widget>[
                Text(snapshot.data.sembuh, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20.0)),
                Text(' Orang'),
              ],
            ),
            subtitle: Text('Sembuh'),
          ),
          ListTile(
            leading: ExcludeSemantics(
              child: CircleAvatar(
                child: Icon(
                  Icons.sentiment_very_dissatisfied,
                  color: Colors.white,
                ),
                backgroundColor: Colors.pink,
              ),
            ),
            title: Row(
              children: <Widget>[
                Text(snapshot.data.meninggal, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20.0)),
                Text(' Orang'),
              ],
            ),
            subtitle: Text('Meninggal'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: futureDataByCountry,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return renderDataItem(snapshot);
        } else if (snapshot.hasError) {
          return Padding(
            padding: const EdgeInsets.all(50.0),
            child: Center(child: Text(snapshot.error)),
          );
        }

        // Default, show loading
        return ShimmerLoading();
      },
    );
  }
}
