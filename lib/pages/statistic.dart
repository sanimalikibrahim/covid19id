import 'package:flutter/material.dart';
import 'package:smi_covid19id/pages/statisticByCountry.dart';
import 'package:smi_covid19id/pages/statisticGlobalDied.dart';
import 'package:smi_covid19id/pages/statisticGlobalPositive.dart';
import 'package:smi_covid19id/pages/statisticGlobalRecover.dart';

class Statistic extends StatelessWidget {
  const Statistic({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scrollbar(
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 8),
          children: [
            Card(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
              child: Container(
                padding: EdgeInsets.all(25),
                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Icon(
                    Icons.insert_chart,
                    size: 20.0,
                    color: Colors.deepOrangeAccent,
                  ),
                  Text(' Statistik', style: TextStyle(fontSize: 20.0)),
                  Text(' Global', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600)),
                ]),
              ),
            ),
            StatisticGlobalPositive(),
            StatisticGlobalRecover(),
            StatisticGlobalDied(),
            Card(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
              child: Container(
                padding: EdgeInsets.all(25),
                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Icon(
                    Icons.insert_chart,
                    size: 20.0,
                    color: Colors.deepOrangeAccent,
                  ),
                  Text(' Statistik', style: TextStyle(fontSize: 20.0)),
                  Text(' Indonesia', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600)),
                ]),
              ),
            ),
            StatisticByCountry(),
            Divider(),
            Container(
              height: 50.0,
              child: Center(
                child: Text('Sumber data:  kawalcorona.com', style: TextStyle(fontSize: 13.0, color: Colors.black54)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
