import 'package:flutter/material.dart';
import 'package:smi_covid19id/pages/about.dart';
import 'package:smi_covid19id/pages/hotline.dart';
import 'package:smi_covid19id/pages/statistic.dart';
import 'package:smi_covid19id/pages/statisticByProvince.dart';
import 'package:smi_covid19id/pages/statisticGlobal.dart';
import 'package:url_launcher/url_launcher.dart';

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final GlobalKey<ScaffoldState> _homeScaffold = GlobalKey<ScaffoldState>();

  void _menuModalCallback() {
    _homeScaffold.currentState.showBottomSheet<void>((BuildContext context) {
      return Container(
        decoration: BoxDecoration(
          color: Colors.white60,
          border: Border(
            top: BorderSide(
              width: 1.0,
              color: Colors.black12,
            ),
            bottom: BorderSide(
              width: 1.0,
              color: Colors.black12,
            ),
          ),
        ),
        height: 215,
        child: Column(
          children: [
            Container(
              height: 50,
              child: Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.white60,
                      blurRadius: 5.0,
                      spreadRadius: 10.0,
                    )
                  ],
                  border: Border(
                    top: BorderSide(
                      width: 1.0,
                      color: Colors.black12,
                    ),
                  ),
                ),
                child: Center(
                  child: Text(
                    'MENU',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
            Divider(thickness: 1),
            Expanded(
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: ExcludeSemantics(
                      child: CircleAvatar(
                        child: Icon(
                          Icons.cloud_circle,
                          color: Colors.black87,
                        ),
                        backgroundColor: Colors.transparent,
                      ),
                    ),
                    title: Row(
                      children: <Widget>[
                        Text('Kasus Per Negara', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0)),
                      ],
                    ),
                    subtitle: Text('Semua'),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => StatisticGlobal(),
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: ExcludeSemantics(
                      child: CircleAvatar(
                        child: Icon(
                          Icons.cloud_circle,
                          color: Colors.black87,
                        ),
                        backgroundColor: Colors.transparent,
                      ),
                    ),
                    title: Row(
                      children: <Widget>[
                        Text('Kasus Per Provinsi', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0)),
                      ],
                    ),
                    subtitle: Text('Indonesia'),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => StatisticByProvince(),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

  void _openWebCallback() async {
    const url = 'https://infeksiemerging.kemkes.go.id/';

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw ('Tidak dapat membuka $url');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        key: _homeScaffold,
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image(
                image: AssetImage('assets/atom_24px.png'),
                height: 24.0,
                fit: BoxFit.contain,
                color: Colors.white,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Covid-19 ID'),
              ),
            ],
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.info_outline),
              tooltip: 'Tentang',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => About(),
                  ),
                );
              },
            ),
          ],
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/double_bubble_outline.png'),
              repeat: ImageRepeat.repeat,
              colorFilter: ColorFilter.mode(Colors.white54.withOpacity(0.6), BlendMode.srcOver),
            ),
          ),
          child: Statistic(),
        ),
        bottomNavigationBar: BottomAppBar(
          shape: const CircularNotchedRectangle(),
          child: Row(
            children: [
              IconButton(
                tooltip: 'Hotline',
                icon: const Icon(Icons.phone_in_talk),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Hotline(),
                    ),
                  );
                },
              ),
              IconButton(
                tooltip: 'Web Kemkes',
                icon: const Icon(Icons.web),
                onPressed: () {
                  _openWebCallback();
                },
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: _menuModalCallback,
          tooltip: 'Filter',
          child: Icon(Icons.menu),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      ),
    );
  }
}
