import 'package:flutter/material.dart';
import 'package:smi_covid19id/component/shimmerLoading.dart';
import 'package:smi_covid19id/services/api.dart';
import 'package:intl/intl.dart';

class StatisticGlobal extends StatefulWidget {
  @override
  _StatisticGlobal createState() => _StatisticGlobal();
}

class _StatisticGlobal extends State<StatisticGlobal> {
  Future<List> futureDataGlobal;

  final _numberFormat = new NumberFormat('#,###');

  void initState() {
    super.initState();
    futureDataGlobal = Api().fetchGlobal();
  }

  Scrollbar renderDataItem(AsyncSnapshot<List> snapshot) {
    return Scrollbar(
      child: ListView.builder(
        itemCount: snapshot.data.length,
        itemBuilder: (BuildContext context, int index) {
          return ListView(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: <Widget>[
              ListTile(
                leading: CircleAvatar(
                  child: Text(
                    "${(index + 1).toString()}",
                    style: TextStyle(color: Colors.white),
                  ),
                  backgroundColor: Colors.red[300],
                ),
                title: Text("${snapshot.data[index].attributes.countryRegion}",
                    style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w500, color: Colors.redAccent)),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("${_numberFormat.format(snapshot.data[index].attributes.confirmed)}",
                        style: TextStyle(fontSize: 15.5, fontWeight: FontWeight.w600, color: Colors.deepOrange)),
                    Text(' Positif'),
                    Text(' / ', style: TextStyle(color: Colors.black12)),
                    Text("${_numberFormat.format(snapshot.data[index].attributes.recovered)}",
                        style: TextStyle(fontSize: 15.5, fontWeight: FontWeight.w600, color: Colors.green)),
                    Text(' Sembuh'),
                    Text(' / ', style: TextStyle(color: Colors.black12)),
                    Text("${_numberFormat.format(snapshot.data[index].attributes.deaths)}",
                        style: TextStyle(fontSize: 15.5, fontWeight: FontWeight.w600, color: Colors.pink)),
                    Text(' Meninggal'),
                  ],
                ),
              ),
              Divider(),
            ],
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Kasus Per Negara')),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/double_bubble_outline.png'),
            repeat: ImageRepeat.repeat,
            colorFilter: ColorFilter.mode(Colors.white54.withOpacity(0.6), BlendMode.srcOver),
          ),
        ),
        child: FutureBuilder(
          future: futureDataGlobal,
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            if (snapshot.hasData) {
              return renderDataItem(snapshot);
            } else if (snapshot.hasError) {
              return Padding(
                padding: const EdgeInsets.all(50.0),
                child: Center(child: Text(snapshot.error)),
              );
            }

            // Default, show loading
            return Container(
              height: 350.0,
              child: Column(
                children: <Widget>[
                  ShimmerLoading(),
                  ShimmerLoading(),
                  ShimmerLoading(),
                  ShimmerLoading(),
                  ShimmerLoading(),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
