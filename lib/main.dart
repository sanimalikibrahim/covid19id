import 'package:flutter/material.dart';
import 'package:smi_covid19id/pages/home.dart';

void main() => runApp(Root());

class Root extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Root',
        theme: ThemeData(
          primarySwatch: Colors.deepOrange,
        ),
        home: Home());
  }
}
